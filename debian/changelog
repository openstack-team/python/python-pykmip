python-pykmip (0.10.0-8) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090595).
  * Add use-assertEqual-not-assertEquals.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 23:33:02 +0100

python-pykmip (0.10.0-7) unstable; urgency=medium

  * Fix compat with python3-cryptography >= 42.0.5 (Closes: #1067798):
    - fix-path-of-load_der_public_key.patch
    - cryptography_39-support.patch

 -- Thomas Goirand <zigo@debian.org>  Thu, 28 Mar 2024 10:32:13 +0100

python-pykmip (0.10.0-6) unstable; urgency=medium

  * Add py3.12-ssl_wrap_socket.patch (Closes: #1058411).

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Dec 2023 17:31:27 +0100

python-pykmip (0.10.0-5) unstable; urgency=medium

  * Cleans better (Closes: #1047705).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2023 14:00:10 +0200

python-pykmip (0.10.0-4) unstable; urgency=medium

  * Add upstream patch: Fix_tests_to_pass_with_SQLAlchemy-1.4.0.patch
    (Closes: #997134).

 -- Thomas Goirand <zigo@debian.org>  Tue, 26 Oct 2021 10:33:46 +0200

python-pykmip (0.10.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 11:28:10 +0200

python-pykmip (0.10.0-2) experimental; urgency=medium

  * Add fix-exception-using-py2-format.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 24 Apr 2020 22:31:04 +0200

python-pykmip (0.10.0-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-yaml as (build-)depends.
  * Standards-Version: 4.5.0.
  * debhelper-compat (= 11).

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 12:48:41 +0200

python-pykmip (0.9.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 10:15:41 +0200

python-pykmip (0.9.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed CVE-2018-1000872 patch applied upstream.
  * Refreshed kill-broken-test.patch.
  * Removed CHANGES.txt from debian/docs.

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Sep 2019 23:11:42 +0200

python-pykmip (0.7.0-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Removed Python 2 support (Closes: #938071).

 -- Thomas Goirand <zigo@debian.org>  Sat, 14 Sep 2019 22:19:45 +0200

python-pykmip (0.7.0-3) unstable; urgency=high

  [ Ondřej Nový ]
  * d/control: Add trailing tilde to min version depend to allow
    backports
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * CVE-2018-1000872: Resource Management Errors (similar issue to
    CVE-2015-5262) vulnerability in PyKMIP server that can result in DOS: the
    server can be made unavailable by one or more clients opening all of the
    available sockets. Applied upstream patch: Fix a denial-of-service bug by
    setting the server socket timeout (Closes: #917030).

 -- Thomas Goirand <zigo@debian.org>  Sun, 24 Feb 2019 17:30:07 +0100

python-pykmip (0.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 20:46:12 +0000

python-pykmip (0.7.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

  [ Thomas Goirand ]
  * New upstream release.
  * Add kill-broken-test.patch.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Feb 2018 22:09:01 +0000

python-pykmip (0.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Ran wrap-and-sort -bast.
  * Fixed (build-)depends versions past Stretch.
  * Standards-Version is now 4.1.1.
  * Using pkgos-dh_auto_install and testing with all Python 3 versions.
  * Fixed python3 shebang to be python3, not python3.x.

 -- Thomas Goirand <zigo@debian.org>  Sun, 05 Nov 2017 20:54:15 +0000

python-pykmip (0.5.0-4) unstable; urgency=medium

  * Team upload.
  * Bumped debhelper compat version to 10
  * Rebuilt with python-sqlalchemy 1.1

 -- Ondřej Nový <onovy@debian.org>  Fri, 02 Dec 2016 22:49:06 +0100

python-pykmip (0.5.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Using OpenStack's Gerrit as VCS URLs.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Oct 2016 14:22:38 +0200

python-pykmip (0.5.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using update-alternatives to manage /usr/bin/pykmip-server.
  * Fixed debian/copyright ordering.
  * Standards-Version is now 3.9.8.

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Jun 2016 09:21:34 +0000

python-pykmip (0.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 10:32:42 +0000

python-pykmip (0.4.0-1) experimental; urgency=medium

  * New upstream release.
  * AUTHORS.txt removed from upstream, removing from debian/docs.

 -- Thomas Goirand <zigo@debian.org>  Sat, 26 Sep 2015 23:53:58 +0200

python-pykmip (0.3.3-2) experimental; urgency=medium

  * Removed (build-)dependency on python3-enum34 (Closes: #796898).

 -- Thomas Goirand <zigo@debian.org>  Fri, 28 Aug 2015 15:30:47 +0000

python-pykmip (0.3.3-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Update gbp configuration for experimental branch.
  * Update watch file to use Debian pypi redirector.

 -- James Page <james.page@ubuntu.com>  Thu, 02 Jul 2015 11:00:51 +0100

python-pykmip (0.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 02 May 2015 22:34:45 +0200

python-pykmip (0.2.0-1) unstable; urgency=medium

  * Initial release. (Closes: #771076)

 -- Thomas Goirand <zigo@debian.org>  Wed, 26 Nov 2014 22:55:22 +0800
